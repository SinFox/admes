FROM ubuntu:20.04

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone

RUN mkdir /opt/admes
WORKDIR /opt/admes

COPY admes.pro /opt/admes/
COPY src /opt/admes/src

RUN apt update \
    && apt install -y --no-install-recommends libqt5core5a libqt5network5 qt5-qmake \
        qt5-qmake-bin qt5-qmake qt5-default libgmp-dev \
        tor torsocks make gcc g++ build-essential \
    && mkdir build \
    && cd build \
    && qmake ../admes.pro\
    && make

ENTRYPOINT [ "/opt/admes/build/admes" ]